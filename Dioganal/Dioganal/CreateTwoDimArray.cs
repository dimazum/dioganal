﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dioganal
{
    class CreateTwoDimArray
    {
        int[,] array;
        public CreateTwoDimArray(int capacity)
        {
            array = new int[capacity, capacity];
        }

        public int[,] FillArray()
        {
            Random rnd = new Random();
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    if (j > i)
                    {
                        array[i, j] = 1;
                    }
                    else
                    {
                        array[i, j] = rnd.Next(1, 9);
                        //array[i, j] = 0;
                    }
                }
            }
            return array;
        }
    }
}
