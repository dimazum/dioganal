﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dioganal
{
    class Program
    {
        static void Main(string[] args)
        {

            CreateTwoDimArray createTwoDimArray = new CreateTwoDimArray(Int32.Parse(Console.ReadLine()));
            OutputConsole outputConsole = new OutputConsole();
            outputConsole.ShowArray(createTwoDimArray.FillArray());

            Console.ReadLine();
        }
    }
}
